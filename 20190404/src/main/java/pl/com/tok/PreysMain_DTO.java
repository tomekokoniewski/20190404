package pl.com.tok;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PreysMain_DTO {

    public static void main(String[] args) {

        String sql = "INSERT INTO preys (species,speed,stodgy,max_weight) values (?,?,?,?)";

        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/pack?serverTimezone=UTC", "ookami", "root")) {
            try (
                    PreparedStatement statement = connection.prepareStatement(sql)) {

                String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                int count = 5;

                for (int i = 0; i < 10; i++) {

                    StringBuilder builder = new StringBuilder();
                    String name;
                    float speed;
                    boolean stodgy;
                    int max_weight;

                    for (int j = 0; j < count; j++) {

                        int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
                        builder.append(ALPHA_NUMERIC_STRING.charAt(character));
                    }

                    name = builder.toString();
                    Random rnd = new Random();
                    speed = rnd.nextFloat();
                    stodgy = rnd.nextBoolean();
                    max_weight = rnd.nextInt();

                    statement.setString(1, name);
                    statement.setFloat(2, speed);
                    statement.setBoolean(3, stodgy);
                    statement.setInt(4, max_weight);

                    statement.executeUpdate();

                }
            } catch (SQLException ex) {
                Logger.getLogger(PreysMain_DTO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ArrayList<Prey> pa = new ArrayList<>();
            try (
                    PreparedStatement statement = connection.prepareStatement(sql)) {

                try (ResultSet result = statement.executeQuery("select * from preys")) {
                    while (result.next()) {
                        Prey p = new Prey(result.getString(1), result.getFloat(2), result.getBoolean(3), result.getInt(4));
                        pa.add(p);
                    }
                }
            }
            System.out.println("-------wypisanie obiektów Pray ---------");
            for (Prey p : pa) {
                System.out.println(p.getSpecies() + "\t" + p.getSpeed() + "\t" + p.isStodgy() + "\t" + p.getMax_weight());
            }
            System.out.println("-------usunięcie ciężkostrawnych -------");
            sql = "delete from preys where stodgy = ?";
            try (
                    PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setBoolean(1, true);
                statement.executeUpdate();
            }
            System.out.println("-------wypisanie obiektów Pray wolniejszych niż 0.5");
            sql = "select * from preys where speed < ?";
            pa.clear();
            try (
                    PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setFloat(1, 0.5F);
                try (ResultSet result = statement.executeQuery()) {

                    while (result.next()) {
                        Prey p = new Prey(result.getString(1), result.getFloat(2), result.getBoolean(3), result.getInt(4));
                        pa.add(p);
                    }
                }

            }
            for (Prey p : pa) {
                System.out.println(p.getSpecies() + "\t" + p.getSpeed() + "\t" + p.isStodgy() + "\t" + p.getMax_weight());
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

    }
}
