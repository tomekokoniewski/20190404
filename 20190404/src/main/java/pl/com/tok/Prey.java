package pl.com.tok;

public class Prey {

private String species;
private float speed;
private boolean stodgy;
private int max_weight;

    @Override
    public String toString() {
        return "Prey{" + "species=" + species + ", speed=" + speed + ", stodgy=" + stodgy + ", max_weight=" + max_weight + '}';
    }

    public Prey(String species, float speed, boolean stodgy, int max_weight) {
        this.species = species;
        this.speed = speed;
        this.stodgy = stodgy;
        this.max_weight = max_weight;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public boolean isStodgy() {
        return stodgy;
    }

    public void setStodgy(boolean stodgy) {
        this.stodgy = stodgy;
    }

    public int getMax_weight() {
        return max_weight;
    }

    public void setMax_weight(int max_weight) {
        this.max_weight = max_weight;
    }
    
}
