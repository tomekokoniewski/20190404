package pl.com.tok;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PreyDao {

    public void add(Prey prey) {

        String sql = "INSERT INTO preys (species,speed,stodgy,max_weight) values (?,?,?,?)";
        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/pack?serverTimezone=UTC", "ookami", "root")) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, prey.getSpecies());
                statement.setFloat(2, prey.getSpeed());
                statement.setBoolean(3, prey.isStodgy());
                statement.setInt(4, prey.getMax_weight());

                statement.executeUpdate();
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

    }

    public void update(Prey prey) {

        String sql = "UPDETE preys set speed = ?, stodgy = ?, max_weight = ? WHERE species = ?";

        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/pack?serverTimezone=UTC", "ookami", "root")) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setFloat(1, prey.getSpeed());
                statement.setBoolean(2, prey.isStodgy());
                statement.setInt(3, prey.getMax_weight());
                statement.setString(4, prey.getSpecies());
                statement.executeUpdate();
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

    }

    public void delete(String name) {

        String sql = "DELETE from preys WHERE species = ?";

        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/pack?serverTimezone=UTC", "ookami", "root")) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, name);
                statement.executeUpdate();
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

    }
    
    public void deleteByStodgy(boolean stodgy) {

        String sql = "DELETE from preys WHERE stodgy = ?";

        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/pack?serverTimezone=UTC", "ookami", "root")) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setBoolean(1, stodgy);
                statement.executeUpdate();
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }
    
    public Prey find(String name) {

        Prey p = new Prey(null, 0, false, 0);
        String sql = "SELECT speed, stodgy, max_weight FROM preys WHERE species = ?";

        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/pack?serverTimezone=UTC", "ookami", "root")) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, name);

                try (ResultSet result = statement.executeQuery()) {

                    while (result.next()) {
                        p.setSpecies(name);
                        p.setSpeed(result.getFloat(1));
                        p.setStodgy(result.getBoolean(2));
                        p.setMax_weight(result.getInt(3));
                    }
                }

            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return p;
    }

    public List<Prey> findAll() {

        ArrayList<Prey> pl = new ArrayList<>();

        String sql = "SELECT species, speed, stodgy, max_weight FROM preys";

        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/pack?serverTimezone=UTC", "ookami", "root")) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {

                try (ResultSet result = statement.executeQuery()) {

                    while (result.next()) {
                        Prey p = new Prey(null, 0, false, 0);

                        p.setSpecies(result.getString(1));
                        p.setSpeed(result.getFloat(2));
                        p.setStodgy(result.getBoolean(4));
                        p.setMax_weight(result.getInt(4));
                        
                        pl.add(p);
                    }
                }

            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return pl;
    }
    
    public List<Prey> findBySlowerThan(float maxSpeed) {

        ArrayList<Prey> pl = new ArrayList<>();

        String sql = "SELECT species, speed, stodgy, max_weight FROM preys WHERE speed < ?";
        
        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/pack?serverTimezone=UTC", "ookami", "root")) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setFloat(1, maxSpeed);
                try (ResultSet result = statement.executeQuery()) {

                    while (result.next()) {
                        Prey p = new Prey(null, 0, false, 0);

                        p.setSpecies(result.getString(1));
                        p.setSpeed(result.getFloat(2));
                        p.setStodgy(result.getBoolean(4));
                        p.setMax_weight(result.getInt(4));
                        
                        pl.add(p);
                    }
                }

            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return pl;
    }

}
