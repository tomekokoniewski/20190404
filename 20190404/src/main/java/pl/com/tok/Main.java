package pl.com.tok;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) {

        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pack?serverTimezone=UTC", "ookami", "root")) {

            Statement st = connection.createStatement();
            ResultSet result = st.executeQuery("Select * from wolves");

            while (result.next()) {
                System.out.println(result.getString(1));
            }
/*
            String query = "INSERT INTO wolves (name) values ('a'),('b'),('c')";
            st.executeUpdate(query);
*/            
            
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }

    }
}
