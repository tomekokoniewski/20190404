package pl.com.tok;


import java.util.Random;


public class PreysMain_DAO {

    public static void main(String[] args) {

                String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                int count = 5;

                for (int i = 0; i < 10; i++) {

                    StringBuilder builder = new StringBuilder();
                    String name;
                    float speed;
                    boolean stodgy;
                    int max_weight;

                    for (int j = 0; j < count; j++) {

                        int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
                        builder.append(ALPHA_NUMERIC_STRING.charAt(character));
                    }

                    name = builder.toString();
                    Random rnd = new Random();
                    speed = rnd.nextFloat();
                    stodgy = rnd.nextBoolean();
                    max_weight = rnd.nextInt();

                Prey p = new Prey(name,speed,stodgy,max_weight);
                PreyDao pd = new PreyDao();
                pd.add(p);
                }
    
            System.out.println("-------wypisanie obiektów Pray ---------");
            PreyDao pd = new PreyDao();
            
            for (Prey p : pd.findAll()) {
                System.out.println(p.toString());
            }
            
            System.out.println("-------usunięcie ciężkostrawnych -------");
            pd.deleteByStodgy(true);
            

            System.out.println("-------wypisanie obiektów Pray wolniejszych niż 0.5");
            for (Prey p : pd.findBySlowerThan(0.5f)) {
                System.out.println(p.toString());
            }
    }
}
